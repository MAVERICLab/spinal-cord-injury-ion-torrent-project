The bioinformatics scripts including quality control, assembly, microbial and viral profiling, and  annotations, used 
in the manuscript named  "Spinal cord injury changes the structure and functional potential of gut bacterial and viral 
communities"  were available at https://bitbucket.org/MAVERICLab/spinal-cord-injury-ion-torrent-project/src/master/.

The original files and the R Markdown document for figures generated in this manuscript are available at 
https://bitbucket.org/MAVERICLab/spinal-cord-injury-ion-torrent-project/downloads/.